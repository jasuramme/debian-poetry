FROM debian:bullseye

# Нам не откуда брать poetry.lock, так как запускаем
# на беслпатных CI/CD серверах, и они не будут хранить наш кэш
# COPY poetry.lock /sandbox/

RUN ln -sf /usr/share/zoneinfo/Europe/Moscow /etc/localtime
RUN apt-get update && apt-get upgrade -y

# кэш удаляется согласно рекомендации 
# https://docs.docker.com/develop/develop-images/dockerfile_best-practices/
RUN apt-get install -y nocache python3.9 python3-pip \
    curl python3.9-dev python3.9-venv git vim && \
    rm -rf /var/lib/apt/lists/*
    
# делаем ссылку, что команда python это python3
RUN [ -f /usr/bin/python ] || ln -s /usr/bin/python3.9 /usr/bin/python

# Устанавливаем poetry
RUN [ -f $HOME/.local/bin/poetry ] || \
    curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py | python -

# Настраиваем poetry
RUN $HOME/.local/bin/poetry config virtualenvs.in-project true

# Установка pyenv
RUN git clone https://github.com/pyenv/pyenv.git ~/.pyenv

# Настройка pyenv
RUN echo 'export PYENV_ROOT=$HOME/.pyenv' > ~/.bashrc
RUN echo 'export PATH=$PYENV_ROOT/bin:$HOME/.local/bin:$PATH' >> ~/.bashrc
RUN echo 'eval "$(pyenv init --path)"' >> ~/.bashrc

CMD ["bash", "--login"]
